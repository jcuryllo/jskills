import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './users.routes';
'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider

    .state('user', {
      url: '/users/:id',
      resolve: {
        user: ($http) => {
          return $http.get('/api/users/123')
            .then(response => {
              return response.data;
            });
        }
      },
      templateUrl: './user.html'
    })
    .state('users', {
      url: '/users',
      resolve: {
        users: ($http) => {
          return $http.get('/api/users')
            .then(response => {
              return response.data;
            });
        }
      },
      controller: ['$scope', 'users', '$window',
        function ($scope, users, $window) {
          $scope.users = users;
          $scope.remove = function (item) {
            if($window.confirm('Are you sure you want to remove this item?') === true) {
              var index = $scope.users.indexOf(item)
              $scope.users.splice(index, 1);
            }
          };
          $scope.star = function (item) {
            item.star = !item.star;
          };
        }],
      template: require('./details.html')
    });

}
