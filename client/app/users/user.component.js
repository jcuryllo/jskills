import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './users.routes';


export default angular.module('jskillsApp.users', [uiRouter])
  .config(routing)
  .component('user', {
    template: require('./details.html'),
    bindings: {
      data: '<'
    }
  })
  .filter('phone', function () {
      return function (phone) {
        if (!phone) {
          return '';
        }

        var value = phone.toString().trim();

        return value.slice(0, 3) + '-' + value.slice(3, 6) + '-' + value.slice(6, 9);
      }
    }
  )
  .name;

