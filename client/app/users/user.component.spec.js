'use strict';

import user from './user.component';
import {
  UserController
} from './user.component';

describe('Component: UserComponent', function() {
  beforeEach(angular.mock.module(user));
  beforeEach(angular.mock.module('stateMock'));

  var scope;
  var userComponent;
  var state;
  var $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function(_$httpBackend_, $http, $componentController, $rootScope, $state) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/user/123')
      .respond();

    scope = $rootScope.$new();
    state = $state;
    userComponent = $componentController('user', {
      $scope: scope
    }, {
      data: {
        first_name: 'Joe',
        last_name: 'Doe',
        position: 'IT Developer',
        avatar: 'http://svgavatars.com/style/svg/11.svg',
        star: false,
        phone: '500 600 700',
        email: 'joe-doe@example.com',
        address: {
          street: 'Grunwaldzka 223',
          city: 'Gdańsk',
          country: 'Polska'
        }
      }
    });
  }));

  it('should expose a `data` object', function() {
    expect(userComponent.user.first_name)
      .toBe('Joe');
    expect(userComponent.user.last_name)
      .toBe('Doe');
  });
});
